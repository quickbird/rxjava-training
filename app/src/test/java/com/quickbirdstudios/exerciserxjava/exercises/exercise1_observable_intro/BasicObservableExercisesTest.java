package com.quickbirdstudios.exerciserxjava.exercises.exercise1_observable_intro;

import com.quickbirdstudios.exerciserxjava.exercises.exercise1_observable_intro.BasicObservableExercises;

import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static io.reactivex.Observable.just;
import static io.reactivex.Observable.range;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BasicObservableExercisesTest {

    private BasicObservableExercises basicObservableExercises = new BasicObservableExercises();

    @Test
    public void exerciseHello() {
        TestObserver<String> ts = new TestObserver<>();
        basicObservableExercises.exerciseHello().subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertValueSequence(Arrays.asList("Hello World!"));
    }

    @Test
    public void exerciseMap() {
        TestObserver<String> ts = new TestObserver<>();
        basicObservableExercises.exerciseMap(just("Hello")).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        assertEquals(1, ts.values().size());
        assertTrue(ts.values().get(0).startsWith("Hello "));
    }

    @Test
    public void exerciseFilterMap() {
        TestObserver<String> ts = new TestObserver<>();
        basicObservableExercises.exerciseFilterMap(range(1, 10)).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertValueSequence(Arrays.asList("2-Even", "4-Even", "6-Even", "8-Even", "10-Even"));
    }

}
