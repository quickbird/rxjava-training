package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3.location_service_example;


import com.quickbirdstudios.exerciserxjava.model.Point;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 19/06/2018.
 */
class RxLocationService {
    private LocationService locationService = new LocationService();

    public Observable<Point> receiveLocationUpdates() {
        return Observable.create(emitter -> locationService.addOnLocationUpdateListener(emitter::onNext));
    }
}
