package com.quickbirdstudios.exerciserxjava.exercises.exercise4_operators_part2;

import com.quickbirdstudios.exerciserxjava.model.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;

public class AdvancedCountriesService {
    /**
     *     !! *** unit tested :-) *** !!
     *     run AdvancedCountriesServiceTest first to see that all tests FAIL
     *     then make them PASS
     */

    public Observable<Long> sumPopulationOfCountries(Observable<Country> countryObservable1,
                                                     Observable<Country> countryObservable2) {
        // TODO 1: return the summed population of all countries from BOTH observables
        // HINT use a combining operator and use "reduce" for summing it up
        // INSPIRATIONAL EXAMPLE OF REDUCE FOR GETTING THE MINIMUM VALLUE OF A LIST:
        // .reduce(0, (currentMin, nextValue) -> Math.min(currentMin,nextValue));


        return null;
    }

    public Single<Boolean> areEmittingSameSequences(Observable<Country> countryObservable1,
                                                    Observable<Country> countryObservable2) {
        // TODO 2: return true if both sequences are identical
        // HINT: there are different ways for archieving this but "zip" and "all" might be of help

        return null;
    }

    // CHALLENGE
    public Observable<Country> listPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty(final FutureTask<List<Country>> countriesFromNetwork) {
        // TODO 3 (EXTRA): PART 1 - return all countries that have a bigger population than 1000000
        // HINT: use flatMapIterable(list -> list) to convert your observables of a country list into an
        //       observable of ONLY countries

        // TODO 3 (EXTRA): PART 2 (challenge) - of a timeout of 3 seconds was exceeded return NO COUNTRY AT ALL
        // TODO 3 (EXTRA): PART 2 (challenge) - use "timeout" and catch the error with "onError..."

        return null;
    }
}
