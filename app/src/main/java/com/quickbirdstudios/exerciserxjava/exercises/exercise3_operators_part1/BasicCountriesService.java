package com.quickbirdstudios.exerciserxjava.exercises.exercise3_operators_part1;

import com.quickbirdstudios.exerciserxjava.model.Country;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class BasicCountriesService {
    /**
     *     !! *** unit tested :-) *** !!
     *     run BasicCountriesServiceTest first to see that all tests FAIL
     *     then make them PASS
     */

    public Single<String> countryNameInCapitals(Country country) {
//    TODO 1 return the country name in capitals
//    TODO 1 a) create single from country b) apply operator c) return the whole thing
        return null;
    }

    public Single<Integer> countCountries(List<Country> countries) {
//    TODO 2 return the country list size
        return null;
    }

    public Observable<Long> listPopulationOfEachCountry(List<Country> countries) {
//    TODO 3 return the population of each country (as an observable)
        return null;
    }

    public Observable<String> listNameOfEachCountry(List<Country> countries) {
//    TODO 4 return the name of each country (as an observable)
        return null;
    }

    public Observable<Country> listOnly3rdAnd4thCountry(List<Country> countries) {
//    TODO 5 return the third and fourth country ONLY
        return null;
    }

    public Single<Boolean> isAllCountriesPopulationMoreThanOneMillion(List<Country> countries) {
//    TODO 6 return if the population of each of all the countries is bigger than 1000000
//    HINT use ".all"
        return null;
    }

    public Observable<Country> listPopulationMoreThanOneMillion(List<Country> countries) {
//    TODO 7 return all countries whose population is bigger than 1000000
        return null;
    }

    public Observable<Long> sumPopulationOfCountries(List<Country> countries) {
//    TODO 8 EXTRA (difficult): return the sum of the countries' pupulations in the list
//    HINT: use reduce
        return null;
    }

    public Observable<String> getCurrencyUsdIfNotFound(String countryName, List<Country> countries) {
//    TODO 9 EXTRA (difficult): return the currency of the given country,
//    TODO 9 if no country matches the given country return "USD"
//    HINT: ".defaultIfEmpty" is useful here
        return null;
    }
}
