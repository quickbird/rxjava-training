package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3;

import com.quickbirdstudios.exerciserxjava.model.Cafe;
import com.quickbirdstudios.exerciserxjava.model.CafePicture;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class NetworkClient {
    public void searchNearbyCafePlaces(String query, CafePlaceSearchFinishedCallback callback) {
        System.out.println("Searching for query: "+query+" ...");

        if (query == null || query.isEmpty())
            callback.onCafePlaceDetailsFetched(Collections.emptyList(),
                    new IllegalArgumentException("Input query is invalid"));

        // mocking the server request
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Cafe> cafes = Arrays.asList(new Cafe("Starbucks", 4.3f),
                new Cafe("SF Coffee Company", 4.1f),
                new Cafe("Tom's Cafe Shop", 4.7f));

        callback.onCafePlaceDetailsFetched(cafes, null);
    }

    public void fetchCafePlacePicture(Cafe cafe, CafePlacePictureFetchCallback callback) {
        System.out.println("Searching for detail of: "+cafe+" ...");

        if (cafe == null)
            callback.onCafePlacePictureFetched(null,
                    new IllegalArgumentException("Input cafe is null"));

        // mocking the server request
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.onCafePlacePictureFetched(new CafePicture("knnfkefn"), null);
    }

    public interface CafePlaceSearchFinishedCallback {
        void onCafePlaceDetailsFetched(List<Cafe> cafe, Throwable throwable);
    }

    public interface CafePlacePictureFetchCallback {
        void onCafePlacePictureFetched(CafePicture cafePicture, Throwable throwable);
    }

}
