package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3;

import com.quickbirdstudios.exerciserxjava.model.Cafe;
import com.quickbirdstudios.exerciserxjava.model.CafePicture;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class RxNetworkClient {
    private final NetworkClient networkClient;

    public RxNetworkClient() {
        this.networkClient = new NetworkClient();
    }

    public RxNetworkClient(NetworkClient networkClient) {
        this.networkClient = networkClient;
    }

    /**
     * !! *** unit tested :-) *** !!
     * run RxNetworkClientTest first to see that all tests FAIL
     * then make them PASS
     */

    public Single<List<Cafe>> searchNearbyCafePlaces(String query) {
        return Single.create(emitter -> {
            /**
             *  TODO 1: return a Single that
             *  TODO 1: - emits an error if the throwable IS NOT null
             *  TODO 1: - emits a Cafe object once it received one
             *  HINT: you can orient at the RxLocationService example
             */

             // add solution here
        });
    }

    public Single<CafePicture> fetchCafePlacePicture(Cafe cafe) {
        /**
         *  TODO 2: return a Single that
         *  TODO 2: - emits an error if the throwable IS NOT null
         *  TODO 2: - emits a CafePicture object once it received one
         *  HINT: you can orient at the RxLocationService example
         */

        return null; // <- add solution here

    }
}
