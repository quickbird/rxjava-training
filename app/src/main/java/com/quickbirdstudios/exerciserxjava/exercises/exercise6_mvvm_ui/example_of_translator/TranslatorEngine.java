package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */

public final class TranslatorEngine {
    private TranslatorEngine() {
        //no instance
    }

    public static String translateToGerman(String english) {
        return english+" translated";
    }
}
