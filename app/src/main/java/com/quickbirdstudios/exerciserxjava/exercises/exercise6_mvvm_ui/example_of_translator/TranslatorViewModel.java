package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public interface TranslatorViewModel {
    // --- inputs
    Observer<String> inputEnglishText();
    Observer<Boolean> inputSaveTrigger();
    Observer<Boolean> inputLogoutTrigger();

    // --- outputs
    Observable<String> outputGermanText();
    Observable<Boolean> outputIsSavingAllowed();
    Observable<String> outputSavedGermanTranslation();
    Observable<Boolean> outputLoggedOut();
}