package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */

public interface LoginViewModel {
    // inputs
    Observer<String> inputEmailText();

    Observer<String> inputPasswordText();

    Observer<Boolean> inputLoginTrigger();

    // outputs
    Observable<Boolean> outputIsLoginButtonEnabled();

    Observable<Boolean> outputIsPasswordValid();

    Observable<Boolean> outputIsEmailValid();

    Observable<Boolean> outputLoggedInTrigger();
}